#!/usr/bin/python
# Filename: diagrams.py

import pandas as pd
import numpy as np
import scipy
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.stats as stats
import datetime

i = datetime.datetime



def plotter_linReg(x , y  , x_label , y_label  , h_cm = 20 , w_cm = 25 , fontsize = 20, axeslabel = 20 , tickslabel = 20 , figtext_pos = [0.2 , 0.8] , fig_text = [r"~y$~~=$" , r'~x' , r'' ] , color = "black"):

    '''
    x : pandas Series for the x-values
    y : pandas series for the y-values
    '''
    
    #Korrektur der Regressionsgerade manuell

    #Präample Einstellungen
    pgf_with_rc_fonts = {
        
        'text.usetex' : True,
        'font.family' : "sans-serif",
        
        'font.size' : fontsize,
        'axes.labelsize' : axeslabel,
        'xtick.labelsize' : tickslabel,
        'ytick.labelsize' : tickslabel,
        'text.latex.preamble' : [r'\usepackage{sfmath}']
    }
    mpl.rcParams.update(pgf_with_rc_fonts)

    #Berechnung der linearen regression
    lin_reg = stats.linregress(x , y) # slope , intercept,r_value ,p_value, std_err
    fit_fn = np.poly1d((lin_reg[0] , lin_reg[1]))

    #print(lin_reg)
    
    absolute_lin_reg = np.absolute(lin_reg)
    sign_lin_reg = np.sign(lin_reg)

    #print(sign_lin_reg)
    
    sign_lin_regstr = []
    for l in range(0,len(sign_lin_reg)-1):
        if sign_lin_reg[l] < 0:
            sign_lin_regstr.append( r"~$+$~")
        elif sign_lin_reg[l] > 0:
            if l > 0:
                sign_lin_regstr.append( r"~$-$~")
            else:
                sign_lin_regstr.append("~")
            
    ########################

    plt.figure(figsize=(w_cm / 2.54 ,  h_cm/2.54))
    ax = plt.subplot()

    #print(sign_lin_regstr)

    ax.plot(x,y,'x',color=color,markersize=8)
    ax.plot(x,fit_fn(x),color =color)
    plt.figtext(figtext_pos[0] , figtext_pos[1] ,fig_text[0] + sign_lin_regstr[0] + "{:0.4f}".format(absolute_lin_reg[0]) + fig_text[1]+ sign_lin_regstr[1] +"{:0.4f}".format(absolute_lin_reg[1]) + fig_text[2]+'\n'
                r'$R^2~=~$' +  "{:0.4f}".format(lin_reg[2]**2)) 


    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    
    space_x_axis = 0.1 * (x.max()-x.min())
    space_y_axis = 0.1 * (y.max()-y.min())
    ax.axis([x.min()-space_x_axis,x.max()+space_x_axis,y.min()-space_y_axis,y.max()+space_y_axis])

    plt.tight_layout(.5)

    
    return [ax , lin_reg]

#data = pd.read_csv("Messdaten_lineare_regression.csv" , sep = "\t" , header = None)
#plot = plotter_linReg(x = data[0] , y =data[1], x_label = r'$C^{-1}$' , y_label = "Hallo" )

def barplot(samples , targets , values  , error
            , filename = "%s_%s_%s" % (i.year, i.month,  i.day) + "-geneexpression.pdf"
            , color = ["#7fc97f","#d8b5b5","#fff395","#5d57a6","#992229"]
            , size_inch = (20 , 10)
            , x_label = "Stadien"
            , y_label = "Expression"
            ,fontsize = 20
            , axeslabel = 20
            , tickslabel = 20
            , ylim = False
            , legend_loc =  'upper left'
            ,targets_order = False):
    '''
    Create a barchart for data across different samples with multiple targets.
    Please change this function to suit your use (change the names of the target genes) 
    samples = pd.Series with the different names of the Samples
    targets = pd.Series with the different names of the targets
    values = pf.Series with the NQR values
    error = pd.Series with the NQR Errors
    filename = String with the name of the diagram file (+".pdf" etc.)
    color = list with the different desired colors
    ylim = correct the ylim of the digram; False : automatic adjustment
    targets_order = list with the names of the targets to define the order; False : use of the order in the Data Frame ([ 'aae3_10454','aae3_04120'  ,'aae3_09164','aae3_06595', 'aae3_13190'])
    '''
    
   
    all_data_df = pd.concat([samples, targets, values, error],axis = 1) # Put together the input to one Data Frame
    
    #print(all_data_df)
    
    fig = plt.figure(figsize=size_inch) #create a chart and set the size in inches
    ax = fig.add_subplot(111) #Creating the axes and the division of only one subdiagram

    #Präample Einstellungen
    pgf_with_rc_fonts = {
        
        'text.usetex' : True,
        'font.family' : "sans-serif",
        
        'font.size' : fontsize,
        'axes.labelsize' : axeslabel,
        'xtick.labelsize' : tickslabel,
        'ytick.labelsize' : tickslabel,
        'text.latex.preamble' : [r'\usepackage{sfmath}']
    }
    mpl.rcParams.update(pgf_with_rc_fonts)
    
    

    #Creates arrays of the occurring parameters
    samples_unique  = samples.unique()

    if targets_order == False:
        targets_unique  = targets.unique()
    else:
        targets_unique = targets_order
    
    
    
    space = 0.1 # define the space between the bars of the samples
    n = len(targets)
    width = ((1 - space) / n)*4 # define the width of the bars; adjustments can make with the multiplicator

    # For loop to create the different bars gene by gene
    for i,cond in enumerate(targets_unique): 

        indeces = range(1, len(samples_unique)+1) # determine the number of the samples 
        vals = all_data_df[all_data_df.iloc[:,1]== cond].iloc[:,2].astype(np.float).tolist() # Values of the parameters of the gene
        
        pos = [j - (1 - space) / 2. + i * width for j in indeces] # determine the position of the bars of the gene in the different samples
        ax.bar(pos, vals, width=width, label=cond, 
               color=color[i] #cm.Accent(float(i)/n)
               , yerr = all_data_df[all_data_df.iloc[:,1]== cond].iloc[:,3].astype(np.float).tolist()
               , ecolor = "black"
               ) # Function to create the bars

    ax.set_xticks(indeces) #create the right number of ticks
    ax.set_xticklabels(samples_unique) # name x-ticks

    
    ax.set_ylabel(y_label) # y-Label
    ax.set_xlabel(x_label)    # x-Label

    if ylim > 0 :       #set the ylim to a define value
        ax.set_ylim(0,ylim)
    
    # define of the legend
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc=legend_loc,ncol = 2)

#data = pd.read_csv("2018_12_10-Auswertung_aller_Daten_raw.csv" , sep=";" ,decimal=",")
#data = data[~data["Expression"].isnull()]
#plot = barplot(data["Sample"] , data["Target"] , data["Expression"] ,data["Expression SEM"])

def plotter(x_values
            ,y_values
            ,x_label
            , y_label
            , figsize = (20 , 10)
            ,lim = False #((0,30),(0,250000))
            ,color = "black"
            ,linewidth = 4
            , linestyle = "-"
            ,marker = "x"
            ,markersize = 10
            ,markeredgewidth =2
            ,fontsize = 20
            ,axeslabel = 20
            ,tickslabel = 20
            ):
    '''
    x_values \t: X values in pandas series
    y_values \t: y values in pandas series
    x_label  \t: string or r'' for the x label
    y_label  \t: string or r'' for the y label
    figsize  \t: tuple with the sizes in inch (width , height)
    lim      \t: lim in a double tuple ((x_0 , x_-1),(y_0 , y_-1))
    color    \t: color of the line and marker
    lineswidth\t: float width of the line
    linestyle \t: style of the line "" , "-" , "--"
    marker  \t : style of the marker ,"x" , "*"
    markersize \t : float, size of the marker
    markerredgewidth \t: float, width of the markerlines
    fontsize \t: general font size
    axeslabel \t : font size of the axes
    tickslabel \t : font size of the ticks
    '''
    #Präample Einstellungen
    pgf_with_rc_fonts = {
        
        'text.usetex' : True,
        'font.family' : "sans-serif",
        
        'font.size' : fontsize,
        'axes.labelsize' : axeslabel,
        'xtick.labelsize' : tickslabel,
        'ytick.labelsize' : tickslabel,
        'text.latex.preamble' : [r'\usepackage{sfmath}']
    }
    mpl.rcParams.update(pgf_with_rc_fonts)

    fig , ax = plt.subplots(figsize = figsize) #create subplot
    
    plt.plot(x_values , y_values , marker = marker , markersize = markersize , markeredgewidth = markeredgewidth , color = color , linewidth = linewidth ,linestyle =linestyle) #create plot

    ax.set_xlabel(x_label) # name x and y labels
    ax.set_ylabel(y_label)

    if lim != False:
        ax.set_xlim(lim[0])
        ax.set_ylim(lim[1])
    
    
#data = pd.read_csv("Messdaten_lineare_regression.csv" , sep = "\t" , header = None)
#plot = plotter(data[0] , data[1], x_label = r'$C^{-1}$' , y_label = "Hallo")

#plt.show()
